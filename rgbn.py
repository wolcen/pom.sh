#!/usr/bin/env python

#Based on Pimoroni sample files
#Hacked up to support N pixels
import sys

import blinkt 


def usage():
    print("Usage: {} <n> <r> <g> <b>".format(sys.argv[0]))
    sys.exit(1)

if len(sys.argv) != 5:
    print("Incorrect argument count")
    usage()

# Exit if non integer value. int() will raise a ValueError
try:
    n, r, g, b = [int(x) for x in sys.argv[1:]]
except ValueError:
    print("Argument type incorrect")
    usage()

# Exit if any of r, g, b are greater than 255
if max(r, g, b) > 255:
    print("RGB value max is 255")
    usage()

# Exit if number of pixels > 8
if n > 8:
    print("Pixel count max is 8")
    usage()

#print("Setting {n} blocks to {r},{g},{b}".format(n=n, r=r, g=g, b=b))

blinkt.set_clear_on_exit(False)

if n==8:
    blinkt.set_all(r, g, b)
else:
    blinkt.set_all(0, 0, 0)
    for i in range(n):
	blinkt.set_pixel(i, r, g, b)

blinkt.show()
