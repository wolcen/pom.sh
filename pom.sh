#!/bin/bash
# SET UP TIMER
pom=`which pom` || `which pom.sh`
visibleticks=false
break_len=5
if [ ! -z $1 ]; then
  max=$1
else
  max=25
fi
timer=$max
# SET MSG
if [ $max -eq $break_len ]; then
  msg="break time remaining"
else
  msg="work time remaining"
fi
# MAIN LOOP
while [ $timer -gt 0 ]
do
  [[ "$visibleticks" = false ]] || notify-send -h string:bgcolor:#4444ff "POM: ${timer}m ${msg}"
  echo ${timer}m ${msg}
  ((timer--))
  sleep 60
done
# START A BREAK?
if [ $max -eq $break_len ];
then
  notify-send -t 300000 'END OF BREAK!'
else
  notify-send -t 300000 'BREAK!'
  $pom 5
fi
