# POM.SH

A couple not too ugly shell scripts for a timer. Originally meant to support Pomodoro, but they do currently do not support break timer yet.

# Requirements

To enable notifications, you need notify-send. Otherwise, output is to the console only. Notify-send is available in libnotify-bin. Your desktop will likely provide a notification-daemon, but you'll need one if not. (e.g. notify-osd)

If you have a Raspberry Pi and a [Pimoroni Blinkt!](https://shop.pimoroni.com/products/blinkt) module, the timer will change the colors. By default:

More than 8 minutes left, a purple bar.
8 minutes to 1 minute, a red bar with the matching number of pixels lit.
Time up! A green bar.

The light bar can be cleared with:
`./rgbn.py 8 0 0 0`

# Usage

Put the script wherever you run your own files from (e.g. ~/bin) and run itwith by itself (for the default 25 minutes), or else run it with the length of time you'd like, in minutes:

`./pom.sh 25`

Tip: If you wish to prevent pom.sh from dying when your shell exits, use nohup. For example, launching in the background:

`nohup ./pom.sh 25 &`

or, if you have the Blinkt:

`./pom-pi.sh 25`

# Future

- Add silent switches (turn off shell output, or per-minute notices)
- Convert script to just python (auto-detect Blinkt support)
- Include break timing and looping
