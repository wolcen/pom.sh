#!/bin/bash
# max_rgb: when there is > 8 min left.
# min_rgb: when there is <= 8 min left.
# last_rgb: break color
min_rgb=(255 0 255)
max_rgb=(255 0 0)
last_rgb=(0 255 0)

min=${1-25}

# Set everything max_rgb:
./rgbn.py 8 ${max_rgb[0]} ${max_rgb[1]} ${max_rgb[2]}

# The main timer loop
while [ $min -gt 0 ]
do
  #notify-send -h string:bgcolor:#4444ff "POM: $min minutes remaining"
  echo $min minutes remaining
  if [ $min -lt 9 ]; then
    #Set x minutes of lights to red:
    ./rgbn.py $min ${min_rgb[0]} ${min_rgb[1]} ${min_rgb[2]}
  fi
  min=$(($min-1))
  sleep 60
done
#Green on completion
./rgbn.py 8 ${last_rgb[0]} ${last_rgb[1]} ${last_rgb[2]}

#Found a random sound on the pi to play too:
aplay /usr/lib/libreoffice/share/gallery/sounds/ok.wav
#notify-send -t 300000 'BREAK!'
